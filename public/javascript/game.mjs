import {
  createElement,
  getChar,
  addClass,
  removeClass
} from "./helper.mjs";

const username = sessionStorage.getItem("username");

const readyBtn = document.getElementById('ready-btn');
const textContainer = document.getElementById('text-container');
const gameTimerDiv = document.getElementById('game-timer');
const commentBubble = document.getElementById("comment-bubble");

if (!username) {
  window.location.replace("/login");
}

export const deactivateUser = () => {
  alert("User with such username already exists");
  sessionStorage.removeItem("username");
  window.location.replace("/login");
}

const logOutBtn = document.getElementById('quit-room-btn');  
const logOut = () => {
  socket.emit("LOG_OUT");
  sessionStorage.removeItem("username");
  window.location.replace("/login");
}
logOutBtn.addEventListener('click', logOut);

export const startGame = (text, gameTimer) => {
  const textElements = text.split('').map(createCharElement);
  const textContainer = document.getElementById('text-container');
  textContainer.innerHTML = "";
  textContainer.append(...textElements);
  gameTimerDiv.innerHTML = gameTimer;

  const keyPressListener = (event) => {
      socket.emit("PLAYER_KEY_PRESS", getChar(event));
  }

  window.addEventListener('keypress', (event) => {
      keyPressListener(event)
  });
}

const toggleReadyStatus = () => {
  socket.emit("TOGGLE_READY_STATUS");
}

export const createPlayerItem = ({
  name,
  isReady,
  progress
}) => {
  const playerItem = createElement({
      tagName: "div",
      className: "player-item",
  });

  const playerInfo = createElement({
      tagName: "div",
      className: "player-info",
  });

  const playerStatus = createElement({
      tagName: "div",
      className: "ready-status",
      attributes: {
          id: `ready-status-${name}`
      }
  })

  addClass(playerStatus, `${isReady? 'ready-status-green': 'ready-status-red'}`);

  const playerName = createElement({
      tagName: "span",
      className: "",
  })

  playerName.innerHTML = `${name} ${name===username ? '(you)' : ''}`

  playerInfo.appendChild(playerStatus);
  playerInfo.appendChild(playerName);

  const playerProgress = createElement({
      tagName: "progress",
      className: "user-progress",
      attributes: {
          max: 100,
          value: progress
      }
  });

  if (progress === 100) {
      addClass(playerProgress, 'finished')
  }

  playerItem.appendChild(playerInfo);
  playerItem.appendChild(playerProgress);

  return playerItem;
};

  
export const createCharElement = (char, index) => {
  const charElement = createElement({
      tagName: "span",
      attributes: {
          id: `char-${index}`
      }
  })
  charElement.innerHTML = char;
  return charElement
}

export const renderPlayers = (players) => {
  const playersContainer = document.getElementById('players-container');
  const readyBtn = document.getElementById('ready-btn');

  if (readyBtn) {
      readyBtn.innerHTML = players.find(player => player.name === username).isReady ? 'Not Ready' : 'Ready';
      readyBtn.addEventListener('click', toggleReadyStatus)
  }

  const allPlayers = players.map(createPlayerItem);
  playersContainer.innerHTML = "";
  playersContainer.append(...allPlayers);
}


export const startCountdown = (counter) => {
  addClass(readyBtn, 'display-none');
  textContainer.innerHTML = counter;
  removeClass(commentBubble, 'display-none');
}

export const updateCountdown = (counter) => {
  textContainer.innerHTML = counter;
}

export const updateGameTimer = (timeLeft) => {
  gameTimerDiv.innerHTML = timeLeft;
}

export const addComment = (comment) => {
  commentBubble.innerHTML = comment;
}

export const updateTextView = currentPosition => {
  const currentCharElement = document.getElementById(`char-${currentPosition-1}`);
  const nextCharElement = document.getElementById(`char-${currentPosition}`);
  removeClass(currentCharElement, 'underline');
  addClass(currentCharElement, 'highlight');
  addClass(nextCharElement, 'underline');
}

export const gameOver = () => {
  removeClass(readyBtn, 'display-none');
  window.removeEventListener('keypress', (event) => {
      keyPressListener(event)
  });
  textContainer.innerHTML = '';
  gameTimerDiv.innerHTML = ''
}

const socket = io("", {
  query: {
      username
  }
});
socket.on("DEACTIVATE_USER", deactivateUser);
socket.on("UPDATE_PLAYERS", renderPlayers);
socket.on("START_COUNTDOWN", startCountdown);
socket.on("UPDATE_COUNTDOWN", updateCountdown);
socket.on("START_GAME", startGame);
socket.on("UPDATE_GAME_TIMER", updateGameTimer);
socket.on("UPDATE_TEXT_VIEW", updateTextView);
socket.on("ADD_COMMENT", addComment);
socket.on("GAME_OVER", gameOver);