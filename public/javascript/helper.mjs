export const createElement = ({ tagName, className, attributes = {} }) => {
    const element = document.createElement(tagName);
  
    if (className) {
      addClass(element, className);
    }
  
    Object.keys(attributes).forEach(key => element.setAttribute(key, attributes[key]));
  
    return element;
  };
  
  export const addClass = (element, className) => {
    const classNames = formatClassNames(className);
    element.classList.add(...classNames);
  };
  
  export const removeClass = (element, className) => {
    const classNames = formatClassNames(className);
    element.classList.remove(...classNames);
  };
  
  export const formatClassNames = className => className.split(" ").filter(Boolean);
  

  export const getChar = (event) => {
    if (event.which == null) {
        if (event.keyCode < 32) return null;
        return String.fromCharCode(event.keyCode)
    }
  
    if (event.which != 0 && event.charCode != 0) {
        if (event.which < 32) return null;
        return String.fromCharCode(event.which);
    }
  
    return null;
  }