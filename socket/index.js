import * as config from "./config";
import players from '../utils/players';
import Countdown from '../utils/countdown';
import GameFacade from '../utils/game';
import {
    COMMENT_TYPE
} from "../constants/comments";

let gameTimer;
let countdownTimer;

export default io => {
    io.on("connection", socket => {
        const game = new GameFacade(socket);
        const username = socket.handshake.query.username;

        if (players.getPlayer(username)) {
            game.emitDeactivatePlayer();
        } else {
            players.addPlayer(username);
            game.emitUpdatePlayers(players);
        }

        socket.on("TOGGLE_READY_STATUS", () => {
            const player = players.getPlayer(username);
            player.isReady = !player.isReady;
            game.emitUpdatePlayers(players);
            game.emitComment(COMMENT_TYPE.PLAYER_READY_STATUS_UPDATE, {
                player
            });

            if (game.isAllPlayersReady(players)) {
                game.emitStartCountdown();
                game.emitComment(COMMENT_TYPE.WELCOME, {
                    players: players.players
                });

                countdownTimer = new Countdown(
                    config.SECONDS_TIMER_BEFORE_START_GAME,
                    (timeLeft) => game.emitUpdateCountdown(timeLeft),
                    () => startGame()
                );
            }
        });

        socket.on("PLAYER_KEY_PRESS", (char) => {
            const player = players.getPlayer(username);

            if (game.isPlayerPrintedCorrectLetter(player, char)) {
                player.increaseGameProgress();
                game.emitUpdatePlayerTextView(player);
                game.emitUpdatePlayers(players);
                if (game.isPlayerCloseToFinish(player)) {
                    game.emitComment(COMMENT_TYPE.CLOSE_TO_FINISH, {
                        player
                    });
                }

                if (game.isPlayerFinished(player)) {
                    gameTimer.stopTimer();
                    finishGame();
                }


            }
        });

        socket.on("LOG_OUT", () => {
            const player = players.getPlayer(username);
            players.removePlayer(username);
            game.emitComment(COMMENT_TYPE.PLAYER_LOGGED_OUT, {
                player
            });
            game.emitUpdatePlayers(players);
        });

        socket.on("disconnect", () => {
            players.removePlayer(username);
            game.emitUpdatePlayers(players);
        });


        const startGame = () => {
            game.setIsPlaying(true);
            game.emitStartGame(players);
            game.emitComment(COMMENT_TYPE.GAME_START);

            gameTimer = new Countdown(
                config.SECONDS_FOR_GAME,
                (timeLeft) => {
                    if (timeLeft && timeLeft % config.SECONDS_FOR_COMMENT_UPDATE === 0) {
                        game.emitComment(COMMENT_TYPE.REGULAR_TIME_UPDATE, {
                            players: players.players,
                            timeLeft
                        });
                    }
                    game.emitUpdateGameTimer(timeLeft);
                },
                () => finishGame()
            );
        };

        const finishGame = () => {
            game.emitGameOver(players);
            game.emitComment(COMMENT_TYPE.GAME_OVER, {
                players: players.players
            });

            game.setIsPlaying(false);
            players.players.forEach(player => player.resetPlayerGameStatus());
            game.emitUpdatePlayers(players);
        }
    });

};