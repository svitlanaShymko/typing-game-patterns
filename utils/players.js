import Player from './player';

class Players {
  constructor () {
    this.players = [];
  }

  addPlayer(username) {
      const player = new Player(username);
      this.players.push(player);
      return this.players;
  }

  getPlayer(username) {
    const player = this.players.find(player => player.name === username);
    if(!player) return false;
    return player;
  }

  removePlayer(username) {
    this.players = this.players.filter(player => player.name !== username);
    debugger;
  }

}

export default new Players();