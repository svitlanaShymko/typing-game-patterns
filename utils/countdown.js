export default class Countdown {
    constructor(timeInterval, onUpdate, onFinish) {
      this.timeLeft = timeInterval;
      this.timer = setInterval(() => {
        this.timeLeft --;
        onUpdate(this.timeLeft);
        if(this.timeLeft <= 0) {
          this.stopTimer();
          onFinish();
        }
      }, 1000);
    }

    getTimeLeft() {
        return this.timeLeft;
      }
  
    stopTimer() {
      clearInterval(this.timer);
    }
  }