export const getResults = players =>{
    return players.sort((a, b) =>{
      return b.progress - a.progress;
  }).map(({name, progress}, index)=>`${index+1}. ${name}-${progress}%`).join("<br/>");
  }

export const formatTime = time => {
let minutes = parseInt(time / 60, 10);
let seconds = parseInt(time % 60, 10);

minutes = minutes < 10 ? "0" + minutes : minutes;
seconds = seconds < 10 ? "0" + seconds : seconds;

return `${minutes}:${seconds}`;
}