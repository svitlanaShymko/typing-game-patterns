export default class Player {
    constructor(username) {
      this.name = username;
      this.isReady = false;
      this.progress = 0;
      this.currentPosition = 0;
      this.text = ''
    }

    resetPlayerGameStatus() {
        this.isReady = false;
        this.progress = 0;
        this.currentPosition=0;
        this.text='';
      }

      setText(text){
          this.text = text;
      }

      getText(){
          return this.text;
      }

      getIsReady(){
          return this.isReady;
      }
  
      getCurrentPosition(){
          return this.currentPosition;
      }

      getProgress(){
          return parseInt((this.currentPosition/this.text.length)*100);
      }

      increaseGameProgress(){
          this.currentPosition++
          this.progress = this.getProgress();
      }

  }