import { COMMENT_TYPE } from "../constants/comments";
import { getResults } from "./helpers";

// FABRIC PATTERN
export default class Commentator{
    constructor(){
    }

    createComment(type, {players, player, timeLeft}){
        switch(type){
            case COMMENT_TYPE.GAME_START:
                return 'Game started!<br/>Good luck, player(s)!'
            case COMMENT_TYPE.PLAYER_READY_STATUS_UPDATE:
                return player.getIsReady() ? `Player ${player.name} is ready` : `Player ${player.name} is not ready`;
            case COMMENT_TYPE.PLAYER_LOGGED_OUT:
                return `Player ${player.name} left the game`;
            case COMMENT_TYPE.WELCOME:
                return `Welcome to the game!<br/> Today we have ${players.length} player(s) competing: <br/>${players.map(player=> `-${player.name}`).join('<br/>')}`
            case COMMENT_TYPE.REGULAR_TIME_UPDATE:
                return `Another 30 seconds of the game passed.<br/> Current score: <br/>${getResults(players)}<br/>${timeLeft} seconds left to finish this game.`;
            case COMMENT_TYPE.CLOSE_TO_FINISH:
                    return `Player: ${player.name} is very close to finish. Only 30 symbols left to type.`
            case COMMENT_TYPE.GAME_OVER:
                return `GAME OVER.<br/></br>Results:<br/>${getResults(players)}`
        }
    }
}