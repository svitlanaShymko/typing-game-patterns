import * as config from '../socket/config';
import { texts } from '../data';
import Commentator from './commentator';
import { formatTime } from './helpers';

// FACADE PATTERN
export default class GameFacade {
  constructor(socket) {
      this.socket = socket;
      this.isPlaying = false;
  }

  getIsPlaying() {
      return this.isPlaying;
  }

  setIsPlaying(isPlaying) {
      this.isPlaying = isPlaying;
  }

  emitDeactivatePlayer() {
      this.socket.emit("DEACTIVATE_USER");
      this.socket.disconnect();
  }

  emitUpdatePlayers({
      players
  }) {
      this.socket.emit("UPDATE_PLAYERS", players);
      this.socket.broadcast.emit("UPDATE_PLAYERS", players);
  }

  isAllPlayersReady({
      players
  }) {
      return players.every(player => player.getIsReady());
  }

  emitStartCountdown() {
      this.socket.emit("START_COUNTDOWN", config.SECONDS_TIMER_BEFORE_START_GAME);
      this.socket.broadcast.emit("START_COUNTDOWN", config.SECONDS_TIMER_BEFORE_START_GAME);
  }

  emitUpdateCountdown(players) {
      this.socket.emit("UPDATE_COUNTDOWN", players);
      this.socket.broadcast.emit("UPDATE_COUNTDOWN", players);
  }

  emitStartGame({
      players
  }) {
      const text = this.getRandomText();
      players.forEach(player => player.setText(text));
      this.socket.emit("START_GAME", text, formatTime(config.SECONDS_FOR_GAME));
      this.socket.broadcast.emit("START_GAME", text, formatTime(config.SECONDS_FOR_GAME));
  };

  getRandomText() {
      return texts[Math.floor(Math.random() * texts.length)];
  }

  emitUpdateGameTimer(time) {
      this.socket.emit("UPDATE_GAME_TIMER", formatTime(time));
      this.socket.broadcast.emit("UPDATE_GAME_TIMER", formatTime(time));
  }

  emitComment(type, {
      players,
      player,
      timeLeft
  } = {}) {
      const comment = new Commentator().createComment(type, {
          players,
          player,
          timeLeft
      });
      this.socket.emit("ADD_COMMENT", comment);
      this.socket.broadcast.emit("ADD_COMMENT", comment);
  }

  emitGameOver() {
      this.socket.emit("GAME_OVER");
      this.socket.broadcast.emit("GAME_OVER");
  }

  emitUpdatePlayerTextView(player) {
      this.socket.emit("UPDATE_TEXT_VIEW", player.getCurrentPosition());
  };

  isPlayerPrintedCorrectLetter(player, key) {
      return player.getText()[player.getCurrentPosition()] === key;
  }

  isPlayerCloseToFinish(player) {
      return player.getText().length - player.getCurrentPosition() === 30;
  }

  isPlayerFinished(player) {
      return player.getProgress() === 100;
  }

}